# GitLab Web IDE

**STATUS:** In Development

A package for bootstrapping GitLab's context-aware Web IDE. Currently, this uses a browser build of [VSCode](https://github.com/microsoft/vscode).

## How to contribute?

Check out the [developer docs](./docs/dev/README.md).

## How to use the example?

You can run the example locally with `yarn start:example` or visit https://gitlab-org.gitlab.io/gitlab-web-ide/.

### For Client-only WebIDE

1. Fill out the startup configuration form, or accept the default values:

   | Field        | Value                   |
   | ------------ | ----------------------- |
   | Type         | `Client only (Default)` |
   | GitLab URL   | `https://gitlab.com`    |
   | Project Path | `gitlab-org/gitlab`     |
   | Ref          | `master`                |

2. Click **Start GitLab Web IDE**

### For Remote Development WebIDE

1. Fill out the startup configuration form

   | Field            | Value                |
   | ---------------- | -------------------- |
   | Type             | `Remote Development` |
   | Remote Authority | `localhost:9888`     |
   | Host Path        | `/home/user/project` |
   | Connection Token | `<token>`            |

- or add query parameters to URL: `?remoteHost=localhost:9888&hostPath=/home/user/project&tkn=password`

2. Click **Start GitLab Web IDE**

## References

Deployments:

- https://gitlab-org.gitlab.io/gitlab-web-ide/

Other projects:

- [GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension)
- [Spike MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79096)
- [Spike playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode)
- [Temp playground project](https://gitlab.com/gitlab-org/frontend/playground/gitlab-vscode-prod)
