export const createError = (msg: string) => new Error(`[gitlab-web-ide] ${msg}`);
