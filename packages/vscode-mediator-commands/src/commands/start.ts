import { gitlab, GitLabClient } from '@gitlab/gitlab-api-client';
import { IFullConfig, StartCommandOptions, StartCommandResponse } from '../types';

type StartCommand = (options?: StartCommandOptions) => Promise<StartCommandResponse>;

const fetchMergeRequestFromConfig = async (config: IFullConfig, client: GitLabClient) => {
  if (!config.mrId) {
    return undefined;
  }

  // If we were given a `mrTargetProject` that's where the `mrId` actually lives
  const mrProjectPath = config.mrTargetProject || config.projectPath;
  return client.fetchMergeRequest(mrProjectPath, config.mrId);
};

const resolveBranchName = (
  ref: string | undefined,
  project: gitlab.Project,
  mergeRequest?: gitlab.MergeRequest,
) => {
  if (ref) {
    return ref;
  }
  if (mergeRequest) {
    return mergeRequest.source_branch;
  }

  return project.default_branch;
};

const createEmptyBranch = (name: string): gitlab.Branch => ({
  commit: {
    created_at: '',
    id: '',
    message: '',
    short_id: '',
    title: '',
    web_url: '',
  },
  web_url: '',
  name,
});

export const commandFactory =
  (config: IFullConfig, client: GitLabClient): StartCommand =>
  async (options: StartCommandOptions = {}): Promise<StartCommandResponse> => {
    const [userPermissions, project, mergeRequest] = await Promise.all([
      client.fetchProjectUserPermissions(config.projectPath),
      client.fetchProject(config.projectPath),
      fetchMergeRequestFromConfig(config, client),
    ]);

    // If there's a ref coming from options, that means the user has selected this new ref
    const branchName = resolveBranchName(options.ref || config.ref, project, mergeRequest);

    if (project.empty_repo) {
      return {
        gitlabUrl: config.gitlabUrl,
        branch: createEmptyBranch(branchName),
        files: [],
        repoRoot: config.repoRoot,
        project,
        userPermissions,
        forkInfo: config.forkInfo,
      };
    }

    const branch = await client.fetchProjectBranch(config.projectPath, branchName);

    const tree = await client.fetchTree(config.projectPath, branch.commit.id);

    const blobs = tree.filter(item => item.type === 'blob');

    const mergeRequestUrl = mergeRequest?.web_url || '';
    const isMergeRequestBranch = Boolean(mergeRequest && branchName === mergeRequest.source_branch);

    return {
      gitlabUrl: config.gitlabUrl,
      branch,
      files: blobs,
      repoRoot: config.repoRoot,
      project,
      isMergeRequestBranch,
      mergeRequestUrl,
      userPermissions,
      forkInfo: config.forkInfo,
    };
  };
