import * as vscode from 'vscode';
import { StartCommandOptions } from '@gitlab/vscode-mediator-commands';
import { createSystems, FileList, IFileSystem } from '@gitlab/web-ide-fs';
import { GitLabFileSearchProvider } from './vscode/GitLabFileSearchProvider';
import { start, ready } from './mediator';
import { GitLabFileContentProvider } from './GitLabFileContentProvider';
import { FileSearcher } from './FileSearcher';
import { GitLabFileSystemProvider } from './vscode/GitLabFileSystemProvider';
import { registerCommands } from './commands';
import { initializeSourceControl } from './scm';
import { FS_SCHEME, GET_STARTED_WALKTHROUGH_ID, WEB_IDE_READY_CONTEXT_ID } from './constants';
import { registerReloadCommand } from './commands/reload';
import { initBranchStatusBarItem } from './ui';
import { showCannotPushCodeWarning } from './ui/showCannotPushCodeWarning';

function initializeFileSystemProvider(
  disposables: vscode.Disposable[],
  fs: IFileSystem,
  isReadonly: boolean,
) {
  const vscodeFs = new GitLabFileSystemProvider(fs);

  disposables.push(
    vscode.workspace.registerFileSystemProvider(FS_SCHEME, vscodeFs, {
      isCaseSensitive: true,
      isReadonly,
    }),
  );
}

function refreshFileView() {
  // why: We need to refresh file view by closing and opening the sidebar.
  //      Otherwise, the file view shows the root folder.
  //      https://gitlab.com/gitlab-org/gitlab-web-ide/-/merge_requests/81#note_1178771600
  return Promise.allSettled([
    vscode.commands.executeCommand('workbench.action.closeSidebar'),
    vscode.commands.executeCommand('workbench.explorer.fileView.focus'),
  ]);
}

/**
 * This is the main function that bootstraps the Web IDE VSCode environment
 */
async function initialize(
  disposables: vscode.Disposable[],
  startOptions: StartCommandOptions = {},
) {
  const startResponse = start(startOptions);

  registerCommands(disposables, startResponse);

  const {
    files,
    branch,
    repoRoot,
    project,
    isMergeRequestBranch,
    mergeRequestUrl = '',
    userPermissions,
    forkInfo,
  } = await startResponse;

  // If user can't push, show warning message
  if (!userPermissions.pushCode) {
    // We don't need to wait for this warning. Just fire and forget.
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    showCannotPushCodeWarning(forkInfo);
  }

  // If we are on the merge request branch, consider the merge request URL assoc with the branch
  const branchMergeRequestUrl = isMergeRequestBranch ? mergeRequestUrl : '';

  const { fs, sourceControl, sourceControlFs } = await createSystems({
    contentProvider: new GitLabFileContentProvider(branch.commit.id),
    gitLsTree: files,
    repoRoot,
  });
  const fileList = new FileList({
    initBlobs: files.map(x => x.path),
    sourceControl,
  }).withCache(fs);

  await initializeFileSystemProvider(disposables, fs, !userPermissions.pushCode);
  disposables.push(
    vscode.workspace.registerFileSearchProvider(
      FS_SCHEME,
      new GitLabFileSearchProvider(new FileSearcher(fileList), repoRoot),
    ),
  );

  await initializeSourceControl(disposables, {
    sourceControl,
    sourceControlFs,
    repoRoot,
    branchName: branch.name,
    commitId: branch.commit.id,
    project,
    branchMergeRequestUrl,
  });

  initBranchStatusBarItem(disposables, branch);

  await refreshFileView();

  await vscode.commands.executeCommand('setContext', WEB_IDE_READY_CONTEXT_ID, true);

  await ready();
}

/**
 * This wraps the main initialize function with a nice VSCode progress bar
 */
function initializeWithProgress(disposables: vscode.Disposable[]) {
  return vscode.window.withProgress(
    {
      cancellable: false,
      location: vscode.ProgressLocation.Notification,
      title: 'Initializing GitLab Web IDE...',
    },
    () => initialize(disposables),
  );
}

function setupUI() {
  return Promise.allSettled([
    vscode.commands.executeCommand('workbench.action.openWalkthrough', GET_STARTED_WALKTHROUGH_ID),
  ]);
}

export async function activate(context: vscode.ExtensionContext) {
  // Lovingly borrowed from https://sourcegraph.com/github.com/microsoft/vscode@3bdea7784d6ef67722967a4cd51179b30e9a1013/-/blob/extensions/git/src/main.ts?L175
  // TODO: Is it okay that disposables are added asynchronously?
  const disposables: vscode.Disposable[] = [];
  context.subscriptions.push(
    new vscode.Disposable(() => vscode.Disposable.from(...disposables).dispose()),
  );

  // why: For now, "reload" has to be initialized separate from other disposables
  //      since it clears out and implicitly recreates the other disposables.
  registerReloadCommand(disposables, initialize);

  await Promise.all([setupUI(), initializeWithProgress(disposables)]);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function deactivate() {}
