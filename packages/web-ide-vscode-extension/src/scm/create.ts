import { IFileStatus } from '@gitlab/web-ide-fs';
import * as vscode from 'vscode';
import {
  COMMIT_COMMAND_ID,
  COMMIT_COMMAND_TEXT,
  SOURCE_CONTROL_ID,
  SOURCE_CONTROL_NAME,
  SOURCE_CONTROL_CHANGES_NAME,
  SOURCE_CONTROL_CHANGES_ID,
  FS_SCHEME,
} from '../constants';
import { ResourceDecorationProvider } from './ResourceDecorationProvider';
import { fromUriToScmUri } from './uri';
import { createStatusViewModel, toResourceState } from './status';
import { ISourceControlViewModel } from './types';

interface SourceControlViewModelOptions {
  repoRoot: string;
  sourceControl: vscode.SourceControl;
  changesGroup: vscode.SourceControlResourceGroup;
  resourceDecorationProvider: ResourceDecorationProvider;
}

class SourceControlViewModel implements ISourceControlViewModel {
  private readonly _repoRoot: string;

  private readonly _sourceControl: vscode.SourceControl;

  private readonly _changesGroup: vscode.SourceControlResourceGroup;

  private readonly _resourceDecorationProvider: ResourceDecorationProvider;

  private _lastStatus: IFileStatus[];

  constructor({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  }: SourceControlViewModelOptions) {
    this._repoRoot = repoRoot;
    this._sourceControl = sourceControl;
    this._changesGroup = changesGroup;
    this._resourceDecorationProvider = resourceDecorationProvider;

    this._lastStatus = [];
  }

  getStatus() {
    return this._lastStatus;
  }

  getCommitMessage() {
    return this._sourceControl.inputBox.value;
  }

  update(statuses: IFileStatus[]) {
    this._lastStatus = statuses;

    const statusVms = statuses.map(x => createStatusViewModel(x, this._repoRoot));

    // why: It might be important to update the resource decorations first before
    // adding the resources themselves to the resource group.
    this._resourceDecorationProvider.update(statusVms);
    this._changesGroup.resourceStates = statusVms.map(statusVm => toResourceState(statusVm));
  }
}

const createVSCodeSourceControl = (disposables: vscode.Disposable[]) => {
  const sourceControl = vscode.scm.createSourceControl(SOURCE_CONTROL_ID, SOURCE_CONTROL_NAME);
  disposables.push(sourceControl);

  sourceControl.acceptInputCommand = {
    command: COMMIT_COMMAND_ID,
    title: COMMIT_COMMAND_TEXT,
    arguments: [],
  };
  // this._sourceControl.quickDiffProvider = this;
  sourceControl.inputBox.enabled = true;
  sourceControl.inputBox.placeholder = 'Commit message';
  sourceControl.actionButton = {
    description: COMMIT_COMMAND_TEXT,
    enabled: true,
    secondaryCommands: [],
    command: {
      title: COMMIT_COMMAND_TEXT,
      command: COMMIT_COMMAND_ID,
    },
  };
  sourceControl.quickDiffProvider = {
    provideOriginalResource(uri: vscode.Uri): vscode.Uri | undefined {
      if (uri.scheme === FS_SCHEME) {
        return fromUriToScmUri(uri, '');
      }

      return undefined;
    },
  };

  return sourceControl;
};

const createVSCodeChangesResourceGroup = (
  disposables: vscode.Disposable[],
  sourceControl: vscode.SourceControl,
) => {
  const changesGroup = sourceControl.createResourceGroup(
    SOURCE_CONTROL_CHANGES_ID,
    SOURCE_CONTROL_CHANGES_NAME,
  );
  changesGroup.hideWhenEmpty = false;
  disposables.push(changesGroup);

  return changesGroup;
};

const createResourceDecorationProvider = (disposables: vscode.Disposable[]) => {
  const resourceDecorationProvider = new ResourceDecorationProvider();

  disposables.push(
    vscode.window.registerFileDecorationProvider(
      resourceDecorationProvider.createVSCodeDecorationProvider(),
    ),
  );

  return resourceDecorationProvider;
};

export const createSourceControlViewModel = (
  disposables: vscode.Disposable[],
  repoRoot: string,
): ISourceControlViewModel => {
  const sourceControl = createVSCodeSourceControl(disposables);
  const changesGroup = createVSCodeChangesResourceGroup(disposables, sourceControl);
  const resourceDecorationProvider = createResourceDecorationProvider(disposables);

  return new SourceControlViewModel({
    repoRoot,
    sourceControl,
    changesGroup,
    resourceDecorationProvider,
  });
};
