import { FileStat, FileType } from 'vscode';
import { IFileStats, FileType as WebIdeFileType } from '@gitlab/web-ide-fs';

export const toVSCodeFileType = (type: WebIdeFileType): FileType => {
  switch (type) {
    case WebIdeFileType.Blob:
      return FileType.File;
    case WebIdeFileType.Tree:
      return FileType.Directory;
    default:
      return FileType.Unknown;
  }
};

export const toVSCodeFileStat = (entry: IFileStats): FileStat => ({
  ...entry,
  type: toVSCodeFileType(entry.type),
});
